var gulp = require('gulp');// поключаем gulp
var	sass = require('gulp-sass'); // подключаем sass

gulp.task('sass', function(){ //создаем таск sass
	return gulp.src(['sass/**/*.sass', 'sass/**/*.scss']) // берем источник
		.pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError)) // преобразуем sass в css
		.pipe(gulp.dest('css/'))
});

gulp.task('watch', function(){
	gulp.watch(['sass/**/*.sass', 'sass/**/*.scss'], ['sass']);
});

gulp.task('default',['watch']);